import { useDispatch, useSelector } from 'react-redux';
import { addAction, multiplyAction, popAction, pushAction, substractAction, divideAction, squareAction, sumAction } from 'state/actions/functionAction';
import { numberAction, numberUndoAction } from 'state/actions/numberAction';
import { selectCurrentNumber } from 'state/selectors/selectCurrentNumber';
import { selectCurrentStack } from 'state/selectors/selectCurrentStack';

import styles from './Calculator.module.css';

const renderStackItem = (value: number, index: number) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();
  const onClickNumber = (n: number) => {
    const action = numberAction(String(n));
    dispatch(action);
  };

  const onClickDecimal = () => {
    const action = numberAction('.');
    dispatch(action);
  };

  const onClickAdd = () => {
    if (currentNumber !== "") {
      const action = pushAction(Number(currentNumber));
      dispatch(action);
      const actionClean = numberAction("");
      dispatch(actionClean);
    }
    const action = addAction();
    dispatch(action);
  };

  const onClickSubstract = () => {
    if (currentNumber !== "") {
      const action = pushAction(Number(currentNumber));
      dispatch(action);
      const actionClean = numberAction("");
      dispatch(actionClean);
    }
    const action = substractAction();
    dispatch(action);
  };

  const onClickMultiply = () => {
    if (currentNumber !== "") {
      const action = pushAction(Number(currentNumber));
      dispatch(action);
      const actionClean = numberAction("");
      dispatch(actionClean);
    }
    const action = multiplyAction();
    dispatch(action);
  };

  const onClickDivide = () => {
    if (currentNumber !== "") {
      const action = pushAction(Number(currentNumber));
      dispatch(action);
      const actionClean = numberAction("");
      dispatch(actionClean);
    }
    const action = divideAction();
    dispatch(action);
  };

  const onClickSquare = () => {
    if (currentNumber !== "") {
      const action = pushAction(Number(currentNumber));
      dispatch(action);
      const actionClean = numberAction("");
      dispatch(actionClean);
    }
    const action = squareAction();
    dispatch(action);
  };

  const onClickSum = () => {
    if (currentNumber !== "") {
      const action = pushAction(Number(currentNumber));
      dispatch(action);
      const actionClean = numberAction("");
      dispatch(actionClean);
    }
    const action = sumAction();
    dispatch(action);
  };

  const onClickIntro = () => {
    const action = pushAction(Number(currentNumber));
    dispatch(action);
    const actionClean = numberAction("");
    dispatch(actionClean);
  };

  const onClickUndo = () => {
    if (currentNumber !== "") {
      const actionUndo = numberUndoAction();
      dispatch(actionUndo);
    } else {
      const action = popAction();
      dispatch(action);
    }
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClickDecimal()}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClickAdd()}>+</button>
        <button onClick={() => onClickSubstract()}>-</button>
        <button onClick={() => onClickMultiply()}>x</button>
        <button onClick={() => onClickDivide()}>/</button>
        <button onClick={() => onClickSquare()}>√</button>
        <button onClick={() => onClickSum()}>Σ</button>
        <button onClick={() => onClickUndo()}>Undo</button>
        <button onClick={() => onClickIntro()}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
