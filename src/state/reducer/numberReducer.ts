import { constants } from "buffer";
import { Reducer } from "redux";
import { AppAction } from "../AppAction";

const NUMBER_ACTION = "NUMBER_ACTION";
const NUMBER = "NUMBER";
const DOT = ".";
const EMPTY = "";

type NumberState = {
  num: string;
  dot: boolean;
};

const initialState: NumberState = {
  num: "",
  dot: false,
};

export const numberReducer: Reducer<NumberState, AppAction> = (
  state = initialState,
  action
) => {
  if (action.type === NUMBER_ACTION) {
    switch (action.function) {
      case NUMBER:
        if (action.n.includes(DOT) && state.num.includes(DOT)) {
          return { num: state.num, dot: state.dot };
        }
        if (action.n.includes(DOT)) {
          return { num: state.num + action.n, dot: true };
        }
        if (action.n === EMPTY) {
          return { num: action.n, dot: false };
        }
        return { num: state.num + action.n, dot: state.dot };
      case "UNDO":
        let newNumber = state.num;
        newNumber = newNumber.slice(0, -1);
        return { num: newNumber, dot: state.dot };
      default:
        return state;
    }
  } else {
    return state;
  }
};
