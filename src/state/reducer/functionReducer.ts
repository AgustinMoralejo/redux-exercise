import { Reducer } from 'redux';
import { AppAction } from 'state';

type FunctionState = {
  stack: number[];
  history: number [][];
};

const initialState: FunctionState = {
  stack: [],
  history: [],
};

export const functionReducer: Reducer<FunctionState, AppAction> = (
  state = initialState,
  action
) => {
    if (action.type == 'FUNCTION_ACTION') {
      let newStack: number[] = [...state.stack];
      let second;
      let first;
      switch (action.function) {
        case 'PUSH':
          newStack.push(action.n);
          return { stack: newStack, history: [...state.history, newStack] };
        case 'POP':
          if (state.history.length < 1) {
            return state;
          }
          let actualStack = state.history.pop();
          if (actualStack == undefined) {
            return state
          }
          return { stack: actualStack, history: [...state.history] };
        case 'ADD':
          if (state.stack.length <= 1) return state;
          second = Number(newStack.pop());
          first = Number(newStack.pop());
          return { stack: [...newStack, first + second], history: [...state.history, [...newStack, first + second]] };
        case 'SUBSTRACT':
          if (state.stack.length <= 1) return state;
          second = Number(newStack.pop());
          first = Number(newStack.pop());
          return { stack: [...newStack, first - second], history: [...state.history, [...newStack, first - second]] };
        case 'MULTIPLY':
          if (state.stack.length <= 1) return state;
          second = Number(newStack.pop());
          first = Number(newStack.pop());
          return { stack: [...newStack, first * second], history: [...state.history, [...newStack, first * second]] };
        case 'DIVIDE':
          if (state.stack.length <= 1) return state;
          second = Number(newStack.pop());
          first = Number(newStack.pop());
          return { stack: [...newStack, first / second], history: [...state.history, [...newStack, first / second]] };
        case 'SQUARE':
          first = Number(newStack.pop());
          return { stack: [...newStack, Math.sqrt(first)], history: [...state.history, [...newStack, Math.sqrt(first)]] };
        case 'SUM':
          const sum = newStack.reduce(
            (previousValue, currentValue) => previousValue + currentValue,
            0
          );
          return { stack: [sum], history: [...state.history, [...newStack, sum]] };
        default:
          return state;
      }
    } else {
        return state;
    }
};
