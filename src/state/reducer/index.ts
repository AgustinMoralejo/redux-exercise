import { combineReducers } from 'redux';

import { numberReducer } from './numberReducer';
import { functionReducer } from './functionReducer';
import { higherOrderReducer } from './higherOrderReducer';

export const rootReducer = higherOrderReducer(
  combineReducers({
    number: numberReducer,
    function: functionReducer,
  })
);
