type functionAction = {
    type: 'FUNCTION_ACTION';
    function: 'PUSH' | 'POP' | 'ADD' | 'SUBSTRACT' | 'MULTIPLY' | 'DIVIDE' | 'SQUARE' | 'SUM';
    n: number;
};

export const pushAction = (num: number): functionAction => ({
    type: 'FUNCTION_ACTION',
    function: 'PUSH',
    n: num,
});

export const popAction = (): functionAction => ({
    type: 'FUNCTION_ACTION',
    function: 'POP',
    n: 0,
});

export const addAction = (): functionAction => ({
    type: 'FUNCTION_ACTION',
    function: 'ADD',
    n: 0
});

export const substractAction = (): functionAction => ({
    type: 'FUNCTION_ACTION',
    function: 'SUBSTRACT',
    n: 0
});

export const multiplyAction = (): functionAction => ({
    type: 'FUNCTION_ACTION',
    function: 'MULTIPLY',
    n: 0
});

export const divideAction = (): functionAction => ({
    type: 'FUNCTION_ACTION',
    function: 'DIVIDE',
    n: 0
});

export const squareAction = (): functionAction => ({
    type: 'FUNCTION_ACTION',
    function: 'SQUARE',
    n: 0
});

export const sumAction = (): functionAction => ({
    type: 'FUNCTION_ACTION',
    function: 'SUM',
    n: 0
});
