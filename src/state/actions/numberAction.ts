type NumberAction = {
    type: "NUMBER_ACTION";
    function: "NUMBER" | "UNDO";
    n: string;
  };
  
export const numberAction = (num: string): NumberAction => ({
    type: "NUMBER_ACTION",
    function: "NUMBER",
    n: num,
});

export const numberUndoAction = (): NumberAction => ({
    type: "NUMBER_ACTION",
    function: "UNDO",
    n: "",
});